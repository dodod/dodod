#!/bin/bash
# Aktualisiert alle Gitlab-Repos in die jeweiligen Odner
# Namenkonvention: Name-des-Fachs/notizen  bwz /uebugsblaetter


# Current scripts location
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# Gehe ins Studium-Root-Verzeichnis
[ -d ~/Studium ] && studdir="$HOME/Studium"
[ -d ~/Git-Repos/Studium ] && studdir="$HOME/Git-Repos/Studium"
[ -d $DIR/../../Studium ] && studdir="$DIR/../../Studium"


toUpdate=(
	"DAP1/notizen"
	"DAP1/zusammenfassung"
	"DAP1/mitschriften"
	"DAP1/uebungsblaetter"
	"Logik/notizen"
  "Logik/uebungsblaetter"
  "Logik/zusammenfassung"
  "Logik/mitschriften"
	"MafI1/notizen"
	"MafI1/uebungsblaetter"
	"MafI1/zusammenfassung"
	"MafI1/mitschriften"
	"RS/notizen"
	"RS/uebungsblaetter"
	"RS/zusammenfassung"
	"RS/mitschriften"
	"misc/zbin"
)

for i in "${toUpdate[@]}"
do
	cd "$studdir/$i"
	git fetch &
	echo "Updating $i..."
done
wait

echo "All Repos fetched..."

for i in "${toUpdate[@]}"
do
	cd "$studdir/$i"
	if [ -z "$(git status | grep diverged)" ]; then
		echo "Updating $i"
		git pull &
		git lfs pull &
	else
		echo "Branches in $i diverged ----- no Pull!"
	fi
done
wait

echo "All done... Have a good day!"
