#!/bin/bash
mkdir -p RS
mkdir -p Logik
mkdir -p MafI1
mkdir -p DAP1
mkdir -p misc

cd DAP1
[ ! -d "./uebungsblaetter" ] && git clone --recursive git@gitlab.com:dodod/dap1-uebungsblaetter.git uebungsblaetter
[ ! -d "./mitschriften" ] && git clone --recursive git@gitlab.com:dodod/dap1-mitschriften.git mitschriften
[ ! -d "./zusammenfassung" ] && git clone --recursive git@gitlab.com:dodod/dap1-zusammenfassung.git zusammenfassung
[ ! -d "./notizen" ] && git clone --recursive git@gitlab.com:dodod/dap1-notizen.git notizen
cd ..

cd RS
[ ! -d "./uebungsblaetter" ] && git clone --recursive git@gitlab.com:dodod/rs-uebungsblaetter.git uebungsblaetter
[ ! -d "./mitschriften" ] && git clone --recursive git@gitlab.com:dodod/rs-mitschriften.git mitschriften
[ ! -d "./zusammenfassung" ] && git clone --recursive git@gitlab.com:dodod/rs-zusammenfassung.git zusammenfassung
[ ! -d "./notizen" ] && git clone --recursive git@gitlab.com:dodod/rs-notizen.git notizen
cd ..

cd MafI1
[ ! -d "./uebungsblaetter" ] && git clone --recursive git@gitlab.com:dodod/mafi1-uebungsblaetter.git uebungsblaetter
[ ! -d "./mitschriften" ] && git clone --recursive git@gitlab.com:dodod/mafi1-mitschriften.git mitschriften
[ ! -d "./zusammenfassung" ] && git clone --recursive git@gitlab.com:dodod/mafi1-zusammenfassung.git zusammenfassung
[ ! -d "./notizen" ] && git clone --recursive git@gitlab.com:dodod/mafi1-notizen.git notizen
cd ..

cd Logik
[ ! -d "./uebungsblaetter" ] && git clone --recursive git@gitlab.com:dodod/logik-uebungsblaetter.git uebungsblaetter
[ ! -d "./mitschriften" ] && git clone --recursive git@gitlab.com:dodod/logik-mitschriften.git mitschriften
[ ! -d "./zusammenfassung" ] && git clone --recursive git@gitlab.com:dodod/logik-zusammenfassung.git zusammenfassung
[ ! -d "./notizen" ] && git clone --recursive git@gitlab.com:dodod/logik-notizen.git notizen
cd ..

cd misc
[ ! -d "./dodod-template" ] && git clone --recursive git@gitlab.com:dodod/dodod-template.git
[ ! -d "./lernmaterial-links" ] && git clone --recursive git@gitlab.com:dodod/lernmaterial-links.git
[ ! -d "./libreoffice-lernplangenerator" ] && git clone --recursive git@gitlab.com:dodod/libreoffice-lernplangenerator.git
[ ! -d "./dodod-web" ] && git clone --recursive git@gitlab.com:dodod/dodod-web.git
[ ! -d "./karteikarten" ] && git clone --recursive git@gitlab.com:dodod/karteikarten.git
[ ! -d "./zbin" ] && git clone --recursive git@gitlab.com:dodod/zbin.git
cd ..

[ ! -d "./dodod" ] && git clone --recursive git@gitlab.com:dodod/dodod.git

echo "done"
exit 0
