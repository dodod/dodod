# dodod
Dodod steht für overengineered studieren an der TU Dortmund. Wir sind eine
Lerngruppe, die etwas zu viel Spaß an CI-Tools und LaTeX hat und deshalb ein
bisschen overengineered studiert. Irgendwie muss man sich ja auch vom Lernen
ablenken! Davon profitiert aber hoffentlich bald auch die restliche Fakultät,
da wir unsere Systeme Stück für Stück OpenSource machen möchten, damit auch
andere die Tools nutzen können. Geil, oder?

## Namensherkunft
Naja, Zufallsgeneratoren sind schon echt toll. Nicht, dass das etwas mit
unserem Namen zutun hat, der Name dodod ist natürlich streng deterministisch
durch ein Team hochbezahlter Marketingexperten entstanden!

## Tolle Dinge
Zu jedem von uns belegten Modul managen wir die Unterlagen in git-Repo's (sihe unten). Außerdem gibt es zu vielen Fächern eine selbst erstellte Zusammenfassung und Karteikarten für [Anki](https://apps.ankiweb.net/). Über `dodod.de/<quicklink>` könnt Ihr schnell auf Seiten von uns oder der Fachschaft Informatik der TU-Dortmund zugreifen

# Links zu den tollen Dingen
(Viele der direkt studiumsbezogenen / fachbezogenen Links dürfen wir aus unterschiedlichen Gründen nicht öffentlich machen. Falls Ihr Zugriff auf diese Repo's wünscht, kontaktiert uns bitte. Wir prüfen dann, ob wir diese für euch sichtabar machen können.)

![](https://gitlab.com/dodod/dodod/raw/master/logo.jpg?inline=false)

## BS
 - [Vorlesung](https://gitlab.com/dodod/bs/vorlesung)
 - [Übungsblätter](https://gitlab.com/dodod/bs/uebungsblaetter)
 - [Mitschriften](https://gitlab.com/dodod/bs/mitschriften)
 - [Zusammenfassung](https://gitlab.com/dodod/bs/zusammenfassung)
## DAP 1
 - [Vorlesung](https://gitlab.com/dodod/dap1/vorlesung)
 - [Übungsblätter](https://gitlab.com/dodod/dap1/uebungsblaetter)
 - [Mitschriften](https://gitlab.com/dodod/dap1/mitschriften)
 - [Zusammenfassung](https://gitlab.com/dodod/dap1/zusammenfassung)
## DAP 2
 - [Vorlesung](https://gitlab.com/dodod/dap2/vorlesung)
 - [Übungsblätter](https://gitlab.com/dodod/dap2/uebungsblaetter)
 - [Mitschriften](https://gitlab.com/dodod/dap2/mitschriften)
 - [Zusammenfassung](https://gitlab.com/dodod/dap2/zusammenfassung)
## ETKT
 - [Vorlesung](https://gitlab.com/dodod/etkt/vorlesung)
 - [Übungsblätter](https://gitlab.com/dodod/etkt/uebungsblaetter)
 - [Mitschriften](https://gitlab.com/dodod/etkt/mitschriften)
## HaPra
 - [Übungsblätter](https://gitlab.com/dodod/hapra)
## IS
 - [Vorlesung](https://gitlab.com/dodod/is/vorlesung)
 - [Übungsblätter](https://gitlab.com/dodod/is/uebungsblaetter)
 - [Mitschriften](https://gitlab.com/dodod/is/mitschriften)
## Logik
 - [Vorlesung](https://gitlab.com/dodod/logik/vorlesung)
 - [Übungsblätter](https://gitlab.com/dodod/logik/uebungsblaetter)
 - [Mitschriften](https://gitlab.com/dodod/logik/mitschriften)
 - [Zusammenfassung](https://gitlab.com/dodod/logik/zusammenfassung)
## MafI 1
 - [Vorlesung](https://gitlab.com/dodod/mafi1/vorlesung)
 - [Übungsblätter](https://gitlab.com/dodod/mafi1/uebungsblaetter)
 - [Mitschriften](https://gitlab.com/dodod/mafi1/mitschriften)
 - [Zusammenfassung](https://gitlab.com/dodod/mafi1/zusammenfassung)
## MafI 2
 - [Vorlesung](https://gitlab.com/dodod/mafi2/vorlesung)
 - [Übungsblätter](https://gitlab.com/dodod/mafi2/uebungsblaetter)
 - [Zusammenfassung](https://gitlab.com/dodod/mafi2/zusammenfassung)
## Management
 - [Vorlesung](https://gitlab.com/dodod/management/vorlesung)
 - [Übungsblätter](https://gitlab.com/dodod/management/uebungsblaetter)
 - [Zusammenfassung](https://gitlab.com/dodod/management/zusammenfassung)
## Rechnerstrukturen
 - [Vorlesung](https://gitlab.com/dodod/rs/vorlesung)
 - [Übungsblätter](https://gitlab.com/dodod/rs/uebungsblaetter)
 - [Mitschriften](https://gitlab.com/dodod/rs/mitschriften)
 - [Zusammenfassung](https://gitlab.com/dodod/rs/zusammenfassung)
## SWT
 - [Vorlesung](https://gitlab.com/dodod/swt/vorlesung)
 - [Übungsblätter](https://gitlab.com/dodod/swt/uebungsblaetter)
 - [Zusammenfassung](https://gitlab.com/dodod/swt/zusammenfassung)
## SWK
 - [Vorlesung](https://gitlab.com/dodod/swk/vorlesung)
 - [Übungsblätter](https://gitlab.com/dodod/swk/uebungsblaetter)
 - [Zusammenfassung](https://gitlab.com/dodod/swk/zusammenfassung)
## Webtech1
 - [Vorlesung](https://gitlab.com/dodod/webtech1/vorlesung)
 - [Übungsblätter](https://gitlab.com/dodod/webtech1/uebungsblaetter)
 - [Zusammenfassung](https://gitlab.com/dodod/webtech1/zusammenfassung)

## Sonstiges
 - [Lernplangenerator](https://gitlab.com/dodod/libreoffice-lernplangenerator)
 - [Rezepte](https://gitlab.com/dodod/rezepte)
 - [Karteikarten](https://gitlab.com/dodod/karteikarten)
 - [CrowdAnki-Diff](https://gitlab.com/dodod/crowdanki-diff)
 - [LaTeX Template](https://gitlab.com/dodod/dodod-template)
 - [Lernmaterial Links](https://gitlab.com/dodod/lernmaterial-links)

# Dokumentation
## Quicklinks
In der `quicklinks.json`-Datei können Quick-Links definiert werden, die
anschließend über `dodod.de/<custom-link>` erreichbar sind. Wird der Link nicht
gefunden, erfolgt automatisch eine Weiterleitung an `oh14.de/<custom-link>`.
Somit können auch alle [oh14.de](https://oh14.de)-Links über
[dodod.de](https://dodod.de) aufgerufen werden.

Das Format der `quicklinks.json`-Datei sollte selbsterklärend sein. Durch
Verwendung einer kommaseparierten Liste, können auch mehrere Links zu einer Weiterleitung führen.

## Makefiles
Mit den Makefiles könnt Ihr mit nur einem Befehl eure LaTeX-Abgabe kompilieren. Die `Makefile-I` kann in einem übergeordneten Verzeichnis platziert werden, um einen `make all` auf alle untergeordneten Makefiles auszuführen. Die `Makefile--II` wird in dem Verzeichnis abgelegt, in dem die Datei (`main.tex`) kompiliert werden soll. Denkt daran, die Dateinamen jeweils in `Makefile` zu ändern.  ;)

## dodod-update Skript
Ein Skript um mehrere git-Repos zentral über einen Befehl zu verwalten. Dazu gehört die initiale Installation, Abfrage des Status (clean/dirty), und parallele updates für alle verwalteten Repo's.

Welche Repo's verwaltet werden wird über eine Datei (dodod_sources) bestimmt. Diese kann und soll von euch auf eure Bedürfnisse angepasst werden.

Eine ausführliche Hilfe zur Anwendung bekommt Ihr mit `dodod -h` oder `dodod subbefehl -h`.

TODO: Wie setzt man eine ENV-Variable

## Setup eines Vorlesungsrepos mit RSS-Feed (Checkliste)

1. Repository gemäß Namensschema erstellen
1. Natives Git LFS von GitLab ausschalten
1. Visibility von GitLab Pages auf `Everyone` stellen, damit der RSS-Feed öffentlich ist.
1. Die Dateien `rss-config.sh`, `.lfsconfig`, `.gitlab-ci.yml`, `.gitignore` und `.gitattributes` aus einem anderen Repo kopieren
1. Die Dateien `rss-config.sh` und `.lfsconfig` auf das aktuelle Repo anpassen
1. Einen Ordner `Videos` erstellen
1. Videos in den Ordner `Videos` herunterladen
